import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_photo_url(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    url = json.loads(response.content)
    city_photo_url = {
        "photo_url": url["photos"][0]["src"]["original"],
    }
    return city_photo_url


def get_location_weather(city, state):
    # get geocooridinates
    params = {
        "q": city + "," + state + ",USA",
        "limit": "1",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct?"
    cooridinate_data = requests.get(url, params=params)
    p_data = json.loads(cooridinate_data.content)
    lat = str(p_data[0]["lat"])
    lon = str(p_data[0]["lon"])
    # use cooridinates to get weather
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather?"
    response = requests.get(url, params=params)
    weather = json.loads(response.content)
    weather_data = {
        "temp": ((weather["main"]["temp"] - 273.15) * 9 / 5 + 32),
        "description": weather["weather"][0]["description"],
    }

    return weather_data
