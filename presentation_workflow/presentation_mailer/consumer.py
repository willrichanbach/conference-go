import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


while True:
    try:
        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="presentation_approval")
            channel.queue_declare(queue="presentation_rejection")
            channel.basic_consume(
                queue="presentation_approval",
                on_message_callback=approval_email,
                auto_ack=True)
            channel.basic_consume(
                queue="presentation_rejection",
                on_message_callback=rejection_email,
                auto_ack=True)
            channel.start_consuming()

        def process_message(ch, method, properties, body):
            print("  Received %r" % body)

        def approval_email(ch, method, properties, body):
            email_body = json.loads(body)
            print(email_body)
            send_mail(
                "Presentation Approved",
                f"{email_body['presenter_name']}, we're happy to tell you that your presentation {email_body['title']} has been accepted",
                "admin@conference.go",
                [email_body['presenter_email']],
                fail_silently=False,
            )

        def rejection_email(ch, method, properties, body):
            email_body = json.loads(body)
            print(email_body)
            send_mail(
                "Presentaion Rejected",
                f"{email_body['presenter_name']}, unfortunately your presentation {email_body['title']} has been rejected",
                "admin@conference.go",
                [email_body['presenter_email']],
                fail_silently=False,
            )

        if __name__ == '__main__':
            try:
                main()
            except KeyboardInterrupt:
                print('Interrupted')
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

    except AMQPConnectionError:

        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
